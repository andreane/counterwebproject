package com.qaagility.controller;

public class Counter {

    public int calculate(int firstNumber, int secondNumber) {
        if (secondNumber == 0){
            return Integer.MAX_VALUE;
	}
        else {
            return firstNumber / secondNumber;
	}
    }

}

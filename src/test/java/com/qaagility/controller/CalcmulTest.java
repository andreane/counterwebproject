import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import com.qaagility.controller.Calcmul;

public class CalcmulTest {

    @Test
    public void testMul() {
        Calcmul calculator = new Calcmul();
        int expected = 18;
        assertEquals(expected, calculator.mul());
    }
}
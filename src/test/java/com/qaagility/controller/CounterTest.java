import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import com.qaagility.controller.Counter;

public class CounterTest {

    @Test
    public void testCalculate() {
        Counter counter = new Counter();
        int expected = 5;
        assertEquals(expected, counter.calculate(10, 2));
    }

    @Test
    public void testCalculateWithZero() {
        Counter counter = new Counter();
        assertEquals(Integer.MAX_VALUE, counter.calculate(10, 0));
    }
}
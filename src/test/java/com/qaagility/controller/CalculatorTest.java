import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import com.qaagility.controller.Calculator;

public class CalculatorTest {

    @Test
    public void testAdd() {
        Calculator calculator = new Calculator();
        int expected = 9;
        assertEquals(expected, calculator.add());
    }
}